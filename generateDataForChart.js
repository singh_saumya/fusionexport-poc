// import CHART_CONF from './generateChartConfig';
// import DATA_CONF from './datasetSeriesConfig';

const CHART_CONF = require('./generateChartConfig.js');
const DATA_CONF = require('./datasetSeriesConfig.js');

const chartListWithoutCategories = ["column2d","bar2d","column3d","bar3d","line","area2d","spline","splinearea","pie2d","doughnut2d","pie3d","doughnut3d"];
// const customChart = function(){
class CustomChart{
  constructor(){
        this.createCategoriesList  = function(data,key){
          var list = [];
          if(data.length > 0){
            data.forEach(function(obj){
                var o = {
                  label : obj[key]
                }
                list.push(o);
            });
          }

          return [
            {
              category : list
            }
          ];
        };
        this.dataForChartWithoutCategories  = function(data,keyList,keyname){
          var list = [];
          data.forEach(function(dataObj){
            keyList.forEach(function(keyObj){
              if(keyObj.seriesname == dataObj[keyname]){
                if(keyObj.color){
                  var o = {
                    label: keyObj.seriesname,
                    value: dataObj[keyObj.seriesname],
                    color: keyObj.color,
                  };
                  list.push(o);

                }
                else{
                  var o = {
                    label: keyObj.seriesname,
                    value: dataObj[keyObj.seriesname]
                  };
                  list.push(o);
                }

              }

            });
          });

          return list;
        };
        this.isDefined  = function(value){
          return (value != undefined && value != null && value.trim().length > 0);
        };
        this.createLineset  = function(data,configObj){
          var list = [];
          var linesetObj = {
            data : []
          };
          var confKeys = Object.keys(configObj.linesetConf);
          confKeys.forEach(function(key){
            var k = configObj.linesetConf[key];
            linesetObj[k] = configObj.linesetConf[k];
          });
          if(data.length > 0){
            data.forEach(function(obj){
                var a = {
                  value : obj[configObj.linesetConf['seriesname']]
                }
                linesetObj.data.push(a);
            });
          }

          list.push(linesetObj);
          return list;
        };
        this.createDataset  = function(data,configObj){
          var list = [], seriesnameList = [];
          if(typeof configObj.datasetSeriesNameList == 'string'){
            seriesnameList = DATA_CONF[configObj.datasetSeriesNameList];
          }
          var seriesnameValues = [];
          var isSameSeriesName = false;
          if(seriesnameList.length == 2){
            seriesnameList.forEach(function(obj){
                seriesnameValues.push(obj.seriesname);
            });
            isSameSeriesName = (seriesnameValues[0] == seriesnameValues[1]) ? true : false;
          }
          seriesnameList.forEach(function(keyObj){
            var mainObj = {};
            mainObj.data = [];
            var objKeys = Object.keys(keyObj);
            for(var i in objKeys){
              mainObj[objKeys[i]] = keyObj[objKeys[i]];
            }
            if(data.length > 0){
              data.forEach(function(obj,i){
                  var a = {
                    value : obj[mainObj.seriesname]
                  }
                  if(mainObj.dataColors != undefined){
                    a.color = mainObj.dataColors[i];
                  }
                  mainObj.data.push(a);
              });
            }
            mainObj.seriesname = (((mainObj.seriesname).replace("monthly_", "")).replace(/_/g, " ")).toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');
            mainObj.seriesname = (mainObj.seriesname.trim().indexOf(' ') == -1) ? mainObj.seriesname.toUpperCase() : mainObj.seriesname;
            mainObj.seriesname = (isSameSeriesName && ((mainObj.hasOwnProperty('renderas') == true) && mainObj.renderas == 'Line'))  ? "Linear ( "+ mainObj.seriesname +" )" : mainObj.seriesname;
            // mainObj.valueposition = "ABOVE";
            // console.log(' mainObj :: ',mainObj);
            list.push(mainObj);
          });
          return list;
        };
        this.generateDataset  = function(data,configObj){
          var self = this;
          var list = [];
          var seriesnameList = DATA_CONF[configObj.keysForDualAndStackedColCharts];
          if(seriesnameList != undefined && seriesnameList.length >=2 ){
                seriesnameList.forEach(function(keylist){
                  configObj.datasetSeriesNameList = keylist;
                  var datasetList = self.createDataset(data,configObj);
                  var obj ={
                    dataset : datasetList
                  };
                  list.push(obj);
                });
          }
          else{
            list = this.createDataset(data,configObj);
          }

          return list;
        };
        this.generateChartConfig = function(customConfig){
          // console.log('new CHART_CONF() :: ',new CHART_CONF());
          var basicConfigFn = new CHART_CONF();
          var basicConfig =  basicConfigFn[customConfig.type]();
          var keysTobeAdded = Object.keys(customConfig.configTobeAdded);
          // var keysTobeRemoved = Object.keys(customConfig.configTobeRemoved);
          if(keysTobeAdded.length > 0){
            keysTobeAdded.forEach(function(key){
              basicConfig[key] = customConfig.configTobeAdded[key];
            })
          }
          if(customConfig.configTobeRemoved.length > 0){
            customConfig.configTobeRemoved.forEach(function(key){
                delete basicConfig[key];
            })
          }

          return basicConfig;

        };
        this.generateData=function(inputConfigs){
          var chartConfigObj = this.generateChartConfig(inputConfigs.chartConfig);
          var dataSourceCategories, dataSourceDataset = [], dataSourceLineset = [];
          var seriesnameList = DATA_CONF[inputConfigs.dataConfig.datasetSeriesNameList];
          var chartType = (chartListWithoutCategories.indexOf(inputConfigs.chartType) > -1) ? "TYPE-1" : ((inputConfigs.dataConfig.keysForDualAndStackedColCharts != undefined && inputConfigs.dataConfig.keysForDualAndStackedColCharts.length >=2) ? "TYPE-2" : "TYPE-3");
          dataSourceCategories = (chartType ==  "TYPE-1") ? [] : this.createCategoriesList(inputConfigs.data,inputConfigs.dataConfig.keyForCategoriesData);
          // dataSourceDataset = (chartType ==  "TYPE-1") ? this.dataForChartWithoutCategories(inputConfigs.data,inputConfigs.dataConfig.keysForSeriesNames) : this.generateDataset(inputConfigs.data,inputConfigs.dataConfig);
          dataSourceDataset = (chartType ==  "TYPE-1") ? this.dataForChartWithoutCategories(inputConfigs.data,seriesnameList,inputConfigs.dataConfig.keyForCategoriesData) : this.generateDataset(inputConfigs.data,inputConfigs.dataConfig);
          dataSourceLineset = (chartType ==  "TYPE-2") ? this.createLineset(inputConfigs.data,inputConfigs.dataConfig): [];
          switch(chartType){
            case "TYPE-1" :
                  return {
                    chart : chartConfigObj,
                    data : dataSourceDataset
                  }
                  break;
            case "TYPE-2" :
                  return {
                    chart : chartConfigObj,
                    categories : dataSourceCategories,
                    dataset : dataSourceDataset,
                    lineset : dataSourceLineset
                  }
                  break;
            default :
                return {
                  chart : chartConfigObj,
                  categories : dataSourceCategories,
                  dataset : dataSourceDataset
                }
          }
        }
  }
}
module.exports = CustomChart;
// export default CustomChart;
