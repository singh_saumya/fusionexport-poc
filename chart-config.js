const config = {
      MONTHLY_USER_CHANGE : {
          settings : {
            "type": "mscombi2d",
            "renderAt": "chart-container_7",
            "width": 800,
            "height": 350,
            "dataFormat": "json",
            "dataSource" : {
              chartId : 'users-1',
              chartType : 'mscombi2d',
              width: 800,
              height: 350,
              data : [],
              chartConfig : {
                configTobeAdded : {
                  "caption" : "User Change",
                  "borderColor": "#C0392B",
                  "showvalues": "0",
                  "showXAxisLine": "1",
                  "baseFontSize": "10",
                  "showSecondaryLimits": "0",
                  "showDivLineSecondaryValue": "0"
                },
                configTobeRemoved : [],
                type : 'STANDARD'
              },
              dataConfig : {
                datasetSeriesNameList : "USER_CHANGE",
                keyForCategoriesData : 'report_month'
              }
            }
        },
        chartName : 'MONTHLY_USER_CHANGE',
        query:"SELECT report_month_number,report_month,monthly_registered_users ,monthly_net_users,monthly_registered_users_with_devices,monthly_deleted_users FROM hubble_services.overview_monthly ORDER BY report_month_number DESC"
      },
      CUMULATIVE_USER_GROWTH : {
        settings : {
           "type": "mscombi2d",
            "renderAt": "chart-container_8",
            "width": 800,
            "height": 350,
            "dataFormat": "json",
            "dataSource": {
              chartId : 'users-3',
              chartType : 'mscombi2d',
              width: 800,
              height: 350,
              data : [],
              chartConfig : {
                configTobeAdded : {
                  caption : "Cummulative User Growth",
                  borderColor:"#28AE5F",
                  "theme": "fusion",
                  "plotHighlightEffect": "fadeout",
                  "reverseLegend": "1",
                  "showValues": "0",
                  "showXAxisLine": "1",
                  "showSecondaryLimits": "0",
                  "showDivLineSecondaryValue": "0",
                  "adjustdiv": "0",
                  "baseFontSize": "10"
                },
                configTobeRemoved : [],
                type : 'STANDARD'
              },
              dataConfig : {
                datasetSeriesNameList : "CUMULATIVE_USER_GROWTH",
                keyForCategoriesData : 'report_month'
              }
            }
        },
        chartName : 'CUMULATIVE_USER_GROWTH',
        query : "SELECT report_month_number,report_month,cumulative_registered_users,cumulative_registered_users_with_devices,cumulative_deleted_users,cumulative_net_users FROM hubble_services.overview_monthly ORDER BY report_month_number DESC"
      },
      MONTHLY_ACTIVE_USERS : {
        settings : {  "type": "mscolumn2d",
          "renderAt": "chart-container_9",
          "width": 800,
          "height": 350,
          "dataFormat": "json",
          "dataSource":{
            chartId : 'users-3',
            chartType : 'mscolumn2d',
            width: 800,
            height: 350,
            data : [],
            chartConfig : {
              configTobeAdded : {
                "caption" : "Active Users",
                "borderColor":"#28AE5F",
                "theme": "fusion",
                "plotHighlightEffect":"fadeout","reverseLegend":"1","showValues":"0","showXAxisLine":"1","showSecondaryLimits":"0","showDivLineSecondaryValue":"0","adjustdiv":"0","baseFontSize":"10"
              },
              configTobeRemoved : [],
              type : 'STANDARD'
            },
            dataConfig : {
              datasetSeriesNameList : "ACTIVE_USERS",
              keyForCategoriesData : 'report_month'
            }
          }
      },
      chartName : 'MONTHLY_ACTIVE_USERS',
      query : "SELECT report_month_number,report_month,active_users FROM hubble_services.overview_monthly ORDER BY report_month_number DESC"
    },
    TILE_CONFIG : {
      query : "SELECT m1.*,  m2.* FROM (SELECT  m1.report_month_number AS report_month_number_1,m1.report_month AS report_month_1,Coalesce(m1.monthly_registered_users, 0) as monthly_registered_users_1, Coalesce(m1.monthly_registered_users_with_devices, 0) as monthly_registered_users_with_devices_1, Coalesce(m1.monthly_net_users, 0) as monthly_net_users_1,Coalesce(m1.active_users, 0) as active_users_1,Coalesce(m1.monthly_new_devices, 0) as monthly_new_devices_1,Coalesce(m1.monthly_net_devices, 0) as monthly_net_devices_1,Coalesce(m1.active_devices, 0) as active_devices_1,Coalesce(m1.monthly_new_subscribers, 0) as monthly_new_subscribers_1,Coalesce(m1.monthly_subscriber_churn, 0) as monthly_subscriber_churn_1,Coalesce(m1.monthly_net_subscribers, 0) as monthly_net_subscribers_1,Coalesce(m1.monthly_new_subscription_revenue, 0) as monthly_new_subscription_revenue_1,Coalesce(m1.monthly_total_revenue, 0) AS monthly_total_revenue_1,Coalesce(m1.monthly_user_paid_conversion_rate, 0) as monthly_user_paid_conversion_rate_1,Coalesce(m1.monthly_arpu, 0) as monthly_arpu_1,Coalesce(m1.cumulative_registered_users, 0) as cumulative_registered_users_1,Coalesce(m1.monthly_total_subscribers, 0) as monthly_total_subscribers_1 FROM hubble_services.overview_monthly m1 WHERE report_month_number = '{MONTH_1}') m1 LEFT OUTER JOIN (SELECT m1.report_month_number AS report_month_number_2, m1.report_month AS report_month_2, Coalesce(m1.monthly_registered_users, 0) as monthly_registered_users_2, Coalesce(m1.monthly_registered_users_with_devices, 0) as monthly_registered_users_with_devices_2, Coalesce(m1.monthly_net_users, 0) as monthly_net_users_2, Coalesce(m1.active_users, 0) as active_users_2, Coalesce(m1.monthly_new_devices, 0) as monthly_new_devices_2, Coalesce(m1.monthly_net_devices, 0) as monthly_net_devices_2, Coalesce(m1.active_devices, 0) as active_devices_2, Coalesce(m1.monthly_new_subscribers, 0) as monthly_new_subscribers_2, Coalesce(m1.monthly_subscriber_churn, 0) as monthly_subscriber_churn_2, Coalesce(m1.monthly_net_subscribers, 0) as monthly_net_subscribers_2, Coalesce(m1.monthly_new_subscription_revenue, 0) as monthly_new_subscription_revenue_2, Coalesce(m1.monthly_total_revenue, 0) AS monthly_total_revenue_2, Coalesce(m1.monthly_user_paid_conversion_rate, 0) as monthly_user_paid_conversion_rate_2, Coalesce(m1.monthly_arpu, 0) as monthly_arpu_2, Coalesce(m1.cumulative_registered_users, 0) as cumulative_registered_users_2, Coalesce(m1.monthly_total_subscribers, 0) as monthly_total_subscribers_2 FROM hubble_services.overview_monthly m1 WHERE report_month_number = '{MONTH_2}') m2 ON monthly_total_revenue_1 IS NOT NULL ORDER BY monthly_total_revenue_1 DESC"

    }
}
module.exports = config;


/*
const config = {
  MONTHLY_USER_CHANGE : {
      settings : {
        "type": "mscombi2d",
        "renderAt": "chart-container_1",
        "width": 800,
        "height": 350,
        "dataFormat": "json",
        "dataSource": {
        "chart": {
          "caption": "Monthly User Change",
            "adjustdiv": "0",
            "theme": "fusion",
            "drawcustomlegendicon": "1",
            "plottooltext": "$label, $seriesname: $dataValue",
            "plotHighlightEffect": "fadeout",
            "reverseLegend": "1",
            "showValues": "0",
            "showXAxisLine": "1",
            "baseFontSize": "10",
            "showSecondaryLimits": "0",
            "showDivLineSecondaryValue": "0"
          },
        "categories" : [],
        "dataset": []
      }
    },
    chartName : 'MONTHLY_USER_CHANGE',
    query:"SELECT report_month_number,report_month,monthly_registered_users ,monthly_net_users,monthly_registered_users_with_devices,monthly_deleted_users FROM hubble_services.overview_monthly ORDER BY report_month_number DESC"
  },
  CUMULATIVE_USER_GROWTH : {
    settings : {
       "type": "mscombi2d",
        "renderAt": "chart-container_2",
        "width": 800,
        "height": 350,
        "dataFormat": "json",
        "dataSource": {
        "chart": {
          "caption": "Cummulative User Growth",
          "theme": "fusion",
          "plotHighlightEffect": "fadeout",
          "reverseLegend": "1",
          "showValues": "0",
          "showXAxisLine": "1",
          "showSecondaryLimits": "0",
          "showDivLineSecondaryValue": "0",
          "adjustdiv": "0",
          "baseFontSize": "10"
          },
        "categories" : [],
        "dataset": []
      }
    },
    chartName : 'CUMULATIVE_USER_GROWTH',
    query : "SELECT report_month_number,report_month,cumulative_registered_users,cumulative_registered_users_with_devices,cumulative_deleted_users,cumulative_net_users FROM hubble_services.overview_monthly ORDER BY report_month_number DESC"
  },
  MONTHLY_ACTIVE_USERS : {
    settings : {  "type": "mscolumn2d",
      "renderAt": "chart-container_3",
      "width": 800,
      "height": 350,
      "dataFormat": "json",
      "dataSource": {
      "chart": {
        "caption": "Monthly Active Users",
        "theme": "fusion",
        "plotHighlightEffect":"fadeout","reverseLegend":"1","showValues":"0","showXAxisLine":"1","showSecondaryLimits":"0","showDivLineSecondaryValue":"0","adjustdiv":"0","baseFontSize":"10"
      },
      "categories" : [],
      "dataset": []
    }
  },
  chartName : 'MONTHLY_ACTIVE_USERS',
  query : "SELECT report_month_number,report_month,active_users FROM hubble_services.overview_monthly ORDER BY report_month_number DESC"
}
}
module.exports = config;
*/
