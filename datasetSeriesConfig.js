const DataConf = {
  USER_CHANGE: [
    {
      "seriesname": 'monthly_net_users',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_registered_users_with_devices',
      "color": '#FDC534',
      "renderas": 'area'
    },
    {
      "seriesname": 'monthly_registered_users',
      "color": '#5D62B5'
    },
    {
      "seriesname": 'monthly_deleted_users',
      "color": '#F2726E'
    }
  ],
  ACTIVE_USERS: [
    {
      "seriesname": 'active_users',
      "color": '#5D62B5'
    }
  ],
  CUMULATIVE_USER_GROWTH: [
    {
      "seriesname": 'cumulative_net_users',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'cumulative_registered_users_with_devices',
      "color": '#FDC534',
      "renderas": 'area'
    },
    {
      "seriesname": 'cumulative_registered_users',
      "color": '#5D62B5'
    },
    {
      "seriesname": 'cumulative_deleted_users',
      "color": '#F2726E'
    }
  ],
  NEW_DEVICES_CHANGE: [
    {
      "seriesname": 'monthly_net_devices',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_deleted_devices',
      "color": '#F2726E',
      "renderas": 'area'
    },
    {
      "seriesname": 'monthly_new_devices',
      "color": "#8DE0AE"
    }
  ],
  ACTIVE_DEVICES: [
    {
      "seriesname": 'monthly_active_devices',
      "color": "#8DE0AE"
    }
  ],
  CUMULATIVE_DEVICE_GROWTH: [
    {
      "seriesname": 'cumulative_net_devices',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'cumulative_deleted_devices',
      "color": '#F2726E',
      "renderas": 'area'
    },
    {
      "seriesname": 'cumulative_registered_devices',
      "color": "#8DE0AE"
    }
  ],
  CUMULATIVE_SUBSCRIBER_GROWTH: [
    {
      "seriesname": 'monthly_total_subscribers',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_subscriber_churn',
      "color": '#F2726E',
      "renderas": 'area'
    }
  ],
  SUBSCRIBER_CHANGE: [
    {
      "seriesname": 'monthly_net_subscribers',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_subscriber_churn',
      "color": '#F2726E',
      "renderas": 'area'
    },
    {
      "seriesname": 'monthly_new_subscribers',
      "color": "#31ABF7"
    }
  ],
  PAID_USER_CONVERSION_RATE: [
    {
      "seriesname": 'monthly_user_paid_conversion_rate',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_user_paid_conversion_rate',
      "color": "#31ABF7"
    }
  ],
  REVENUE_CHANGE: [
    {
      "seriesname": 'monthly_new_subscription_revenue',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_new_subscription_revenue',
      "color": "#cf83d3"
    }
  ],
  REVENUE_GROWTH: [
    {
      "seriesname": 'monthly_total_revenue',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_total_revenue',
      "color": "#cf83d3"
    }
  ],
  ARPU: [
    {
      "seriesname": 'monthly_arpu',
      "color": '#6DD4D2',
      "renderas": 'Line'
    },
    {
      "seriesname": 'monthly_arpu',
      "color": "#cf83d3"
    }
  ],
  AWS_COST_MONTHLY: [
    {
      "seriesname": 'aws_production_cost',
      "color": '#00b0f0',
    },
    {
      "seriesname": 'aws_development_cost',
      "color": '#c00000',
    },
    {
      "seriesname": 'digitalocean_production_cost',
      "color": '#ffc000',
    },
    {
      "seriesname": 'omegha_cloud_cost',
      "color": '#3465a4',
    },
    {
      "seriesname": 'total_cloud_cost',
      "color": '#4472C4',
      "renderas": 'Line'

    },



  ],
  AWS_COST_PER_DAY: [
    {
      "seriesname": 'cloud_cost_per_day',
      "color": '#00b0f0',
    },
    {
      "seriesname": 'cloud_cost_per_day',
      "color": '#4472C4',
      "renderas": 'Line'
    },
  ],

  COST_DISTRIBUTION: [
    {
      "seriesname":'Baseline Cost',
      "color": "#4170C0"
    },
    {
      "seriesname":'Cloud Recording Cost',
      "color" : '#5B99D4',
    },{
      "seriesname":'Daily Summary Cost',
      "color" : '#254177',
    },{
      "seriesname":'Debugging Cost',
      "color": "#FABD00"
    },
    {
      "seriesname":'Development Cost',
      "color" : '#984614',
    },
    {
      "seriesname":'Events Timeline Cost',
      "color" : '#E67E31',
    },
    {
      "seriesname":'Relay Streaming Cost',
      "color" : '#A3A3A3',
    },
    {
      "seriesname":'Website Cost',
      "color" : '#74A664',
    },
  ],

  BASE_LINE_COST: [
    {
      "seriesname": 'per_device_monthly_cost',
      "dataColors": ['#cccccc', '#ff860d', '#3465a4']
    }

  ],
  CLOUD_RECORDING_COST: [
    {
      "seriesname": 'value_3',
      "color": '#cccccc',
    },

    {
      "seriesname": 'value_2',
      "color": '#ff860d',
    },

    {
      "seriesname": 'value_1',
      "color": '#3465a4',
    },



  ]

}
module.exports = DataConf;
// export default DataConf;
