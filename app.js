// ** IMPORT AND RESOLVE DEPENDENCIES ***

// Import 'path' core module of Node.js

const path = require('path');
const { Pool, Client } = require('pg');
const CHART_CONFIG = require('./chart-config.js');
const GenerateChartDataSource = require('./generateDataForChart.js');

// Import FusionExport SDK client for Node.js
const {
    ExportManager,
    ExportConfig
} = require('fusionexport-node-client');

var connectConfig = {
   host     : 'hubble-data-warehouse.caemv1lkjljq.us-east-1.redshift.amazonaws.com',
   port : '5439',
       user     : 'h2o',
       database : 'h2o',
       password : 'H2o12345'
 };
 const connectionString = 'postgresql://'+connectConfig.user+':'+connectConfig.password+'@'+connectConfig.host+':'+connectConfig.port+'/'+connectConfig.database;
 const pool = new Pool({
    connectionString: connectionString,
  })


const getReportMonth = function(year,month){
  if(month <= 0){
    if(month == 0){
      month =  11;
    }
    else if (month == -1) {
      month = 10;
    }
    year = year-1;
  }
  var str = year+''+(month.toString().length == 1 ? '0'+month : month );
  return str;
}
const constructTileConfig = function (inputObj){
  var a =inputObj.m1 != null ? inputObj.m1 : 0;
  var b =inputObj.m2 != null ? inputObj.m2 : 0;
  var diff = 0;
  if(a != null && b != null){
    diff = ((b == 0 || b == null) && (a != 0 && a != null))  ? 100 : parseFloat((((parseFloat(a)-parseFloat(b))/Math.abs(parseFloat(b)))*100).toFixed(2));
  }

  inputObj.m1 =(inputObj.keyName.indexOf('conversion_rate') == -1) ?  a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : (parseFloat(a*100)).toFixed(2)+'%';
  inputObj.m2 =(inputObj.keyName.indexOf('conversion_rate') == -1) ?  b.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : (parseFloat(b*100)).toFixed(2)+'%';
  var colorCode = Math.sign(diff) == -1 ? '#C0392B' : (parseInt(diff) == 0 ? '#FDD966' : '#28AE5F');
  var config = {
      type: 'column2d',
      renderAt: inputObj.containerId,
      width: '200',
      height: '150',
      dataFormat: 'json',
      dataSource: {
        "chart": {

          "theme": "fusion",
          showYAxisvalue : 0,
          showXAxisvalue : 0,
          divLineAlpha:0
        },
         "annotations": {

          "groups": [{
            "items": [
            {
              "id": "dyn-labelBG",
              "type": "rectangle",
              "radius": "5",
              "x": "$canvasEndX - 150",
                  "y": "$canvasStartY",
                  "tox": "$canvasEndX",
                  "toy": "$canvasStartY + 105",
              "color": colorCode
            },
            {
              "id": "dyn-label1",
              "type": "text",
              "fillcolor": "#ffffff",
              "fontsize": "18",
              "x": "110",
              "y": "35",
              "text": inputObj.title
            },
            {
              "id": "dyn-labelB",
              "type": "rectangle",
              "x":"$canvasEndX - 150",
                  "y": "54",
                  "tox": "$canvasEndX",
                  "toy": "$canvasStartY +40",
              "color": "#ffffff"
            },
            {
              "id": "dyn-label2",
              "type": "text",
              "fillcolor": "#ffffff",
              "fontsize": "20",
              "x": "110",
              "y": "70",
              "text": inputObj.m1
            },
            {
              "id": "dyn-label3",
              "type": "text",
              "fillcolor": "#ffffff",
              "fontsize": "16",
              "x": "110",
              "y": "90",
              "text": inputObj.m2
            },
            {
              "id": "dyn-label4",
              "type": "text",
              "fillcolor": "#ffffff",
              "fontsize": "12",
              "x": "110",
              "y": "110",
              "text": diff+'%'
            }]
          }]
      },
        "data": [{
            "label": "",
            "value": "0",
            "color":"#ffffff"
          }
        ]

      }
    };
  return config;
}
const generateReport = function(){
  var responseConfigArr = [];
  var keysForTiles = ['monthly_registered_users','monthly_net_users','active_users','active_devices','monthly_arpu','monthly_total_revenue'];
  var tileTitleObj =  {
    'monthly_registered_users' : 'Registered Users','monthly_net_users' : 'Net Users','active_users' : 'Active Users',
    'active_devices' : 'Active Devices','monthly_arpu' : 'ARPU','monthly_total_revenue' : 'Total Revenue'
  };
  var querysetArr = [];
  var chartConfigKeys = Object.keys(CHART_CONFIG);
  var chartConfArr = [];
  for(var i in chartConfigKeys){
    querysetArr.push(CHART_CONFIG[chartConfigKeys[i]].query);
    chartConfArr.push(CHART_CONFIG[chartConfigKeys[i]]);
  }

  // console.log('chartConfArr :: ',JSON.stringify(chartConfArr));
  // console.log('querysetArr :: ',JSON.stringify(querysetArr));

  chartConfArr.forEach(function(item,j){
    var queryStr = item.query;
    if(queryStr.indexOf('{') > -1){
        var date = new Date();
        var month1 = getReportMonth(date.getFullYear(),date.getMonth());
        var month2 = getReportMonth(date.getFullYear(),(date.getMonth()-1));
        queryStr = queryStr.replace('{MONTH_1}',month1);
        queryStr = queryStr.replace('{MONTH_2}',month2);
    }

        pool.query(queryStr, (err, response) => {
              if(err){
                console.log('RED-SHIFT QUERY FAILED :: ',err)
              }
              // console.log('response :: ',JSON.stringify(response.rows));
              response.rows = response.rows.reverse();
              if(item.settings != undefined){
                item.settings.dataSource.data = response.rows;

                var customDataFn = new GenerateChartDataSource();
                  var sampleDataOutput = customDataFn.generateData(item.settings.dataSource);
                  // console.log('sampleDataOutput ::  ',JSON.stringify(sampleDataOutput));
                  // console.log('sampleDataOutput ::  ',sampleDataOutput);
                  item.settings.dataSource = sampleDataOutput;
                  // console.log('item.settings.dataSource ::  ',item.settings.dataSource);
                responseConfigArr.push(item.settings);

              }
              else{

                var responseObj = response.rows[0];
                  keysForTiles.forEach(function(key,index){
                    var obj = {
                      m1 : responseObj[key+'_1'],
                      m2 : responseObj[key+'_2'],
                      keyName : key,
                      title :tileTitleObj[key],
                      containerId : 'chart-container_'+(index+1)
                    }
                    var tileConfigObj = constructTileConfig(obj);

                    responseConfigArr.push(tileConfigObj);
                  })
              }
              // console.log(' ELSE BLOCK :: ',JSON.stringify(tileConfigObj));
              // console.log('responseConfigArr :: ',JSON.stringify(responseConfigArr),querysetArr.length,responseConfigArr.length == querysetArr.length);
              console.log('responseConfigArr :: ',responseConfigArr.length,querysetArr.length,responseConfigArr.length == querysetArr.length);

              if((responseConfigArr.length-(keysForTiles.length-1)) == querysetArr.length){
                // responseConfigArr.push(tileConfig1,tileConfig2,tileConfig3);
                // console.log('responseConfigArr :: ',responseConfigArr);
                var timeStamp = new Date();
                timeStamp = ((timeStamp.getUTCDate()).toString().length > 1 ? timeStamp.getUTCDate() : ('0'+timeStamp.getUTCDate()))+'-'+((timeStamp.getUTCMonth()+1).toString().length > 1 ? timeStamp.getUTCMonth()+1 : ('0'+(timeStamp.getUTCMonth()+1)))+'-'+timeStamp.getUTCFullYear()+'';

                // timeStamp = timeStamp.toUTCString();
                // timeStamp = timeStamp.getUTCDate()+''+timeStamp.getUTCMonth()+''+timeStamp.getUTCFullYear()+''+timeStamp.getUTCHours()+''+timeStamp.getUTCMinutes()+''+timeStamp.getUTCSeconds()+''+timeStamp.getUTCMilliseconds()+''+timeStamp.getTimezoneOffset()
                  // timeStamp = ((timeStamp.getUTCDate()).toString().length > 1 ? timeStamp.getUTCDate() : ('0'+timeStamp.getUTCDate()))+'-'+((timeStamp.getUTCMonth()+1).toString().length > 1 ? timeStamp.getUTCMonth()+1 : ('0'+(timeStamp.getUTCMonth()+1)))+'-'+timeStamp.getUTCFullYear()+'::'+((timeStamp.getUTCHours()).toString().length > 1 ? timeStamp.getUTCHours() : ('0'+timeStamp.getUTCHours()))+':'+((timeStamp.getUTCMinutes()).toString().length > 1 ? timeStamp.getUTCMinutes() :('0'+timeStamp.getUTCMinutes()))+':'+((timeStamp.getUTCSeconds()).toString().length > 1 ? timeStamp.getUTCSeconds() : ('0'+timeStamp.getUTCSeconds()))+'';
                const exportConfig = new ExportConfig();
                exportConfig.set('chartConfig',responseConfigArr);
                // exportConfig.set('type', 'pdf');
                // exportConfig.set('quality', 'best');

                var outputFileName = 'export_'+timeStamp;
                console.log('outputFileName :: ',outputFileName);
                exportConfig.set('type', 'html');
                exportConfig.set('templateFilePath', path.join( 'resources', 'sample-template.html'));
                exportConfig.set('dashboardLogo', path.join(__dirname, 'resources', 'hubble-logo.png'));
                exportConfig.set('outputFile',outputFileName);

                // exportConfig.set('dashboardLogo', path.join('https://app.hubbleconnected.com/assets/img/hubble-logo.png'));
                exportConfig.set('dashboardHeading', 'Hubble Control Center');
                exportConfig.set('dashboardSubheading', 'Monthly Report ');
                // console.log('exportConfig  :: ',JSON.stringify(exportConfig.templateFilePath),exportConfig.templateFilePath);
                const exportManager = new ExportManager();
                exportManager.export(exportConfig, outputDir = '.', unzip = true).then((exportedFiles) => {
                  exportedFiles.forEach(file => console.log(file));
                }).catch((err) => {
                  console.log(err);
                });
                pool.end();
              }

            })

  });
/*  const exportConfig = new ExportConfig();
  exportConfig.set('chartConfig',responseConfigArr);
  exportConfig.set('type', 'pdf');
  exportConfig.set('quality', 'best');


  // exportConfig.set('type', 'html');
  // exportConfig.set('templateFilePath', path.join( 'resources', 'sample-template.html'));

console.log('exportConfig  :: ',JSON.stringify(exportConfig.templateFilePath),exportConfig.templateFilePath);
  const exportManager = new ExportManager();
  exportManager.export(exportConfig, outputDir = '.', unzip = true).then((exportedFiles) => {
    exportedFiles.forEach(file => console.log(file));
  }).catch((err) => {
    console.log(err);
  });
  */
}
generateReport();

// ** EXPORT CONFIG ***

// Instantiate ExportConfig and add the required configurations

/* UNCOMMENT THIS -
const exportConfig = new ExportConfig();
*/


// Provide path of the chart configuration which we have defined above.
// You can also pass the same object as serialized JSON.
// exportConfig.set('chartConfig', path.join(__dirname, 'resources', 'chart-config-file.json'));

/* UNCOMMENT THIS -
exportConfig.set('chartConfig',sampleData);
*/

// ** EXPORT-MANAGER ***

// Instantiate ExportManager
/* UNCOMMENT THIS -
const exportManager = new ExportManager();
*/
// console.log('exportConfig :: ',exportConfig.chartConfig);
// * OUTPUT **

// Provide the exportConfig. By default it returns a promise.
// Optionally, print the exported file names and error messages, if any
/* UNCOMMENT THIS -
exportManager.export(exportConfig, outputDir = '.', unzip = true).then((exportedFiles) => {
  exportedFiles.forEach(file => console.log(file));
}).catch((err) => {
  console.log(err);
});
*/
