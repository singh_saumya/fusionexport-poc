class ChartConf{
  constructor(){
    this.STANDARD = function(){
        return {
           "adjustdiv": "0",
           "baseFontSize": "10",
           "borderAlpha": "80",
           "borderThickness": "8",
           "caption": "Report",
           "drawcustomlegendicon": "1",
           "plottooltext": "$label, $seriesname: $dataValue",
           "reverseLegend": "1",
           "showBorder": "0",
           "showDivLineSecondaryValue": "0",
           "showSecondaryLimits": "0",
           "showValues": "0",
           "showXAxisLine": "1",
           "theme": "fusion",
           "animation":"0"
         };
      }
   }
}
// export default ChartConf;
module.exports = ChartConf;
